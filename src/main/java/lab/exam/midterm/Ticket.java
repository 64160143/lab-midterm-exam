package lab.exam.midterm;

public class Ticket {
    private char sym;
    private int x;
    private int y; 
    public Ticket(int x, int y){
        this.sym = 'X';
        this.x = x;
        this.y = y;
    }


    public int getX(){
        return x;

    }

    public int getY(){
        return y;
    }

    public char getsym(){
        return sym;
    }

    public void print(){
        System.out.println("Ticket" + sym + "(" + x +","+ y + ")");
    }
    public boolean ison(int x ,int y){
        return this.x == x && this.y == y;
    }

}
